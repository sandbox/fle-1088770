<?php

/**
 * Create a new slideshow_block
 * @param $form
 * @param $form_state
 * @return form
 */
function slideshow_block_add_block($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  // Getting block form
  $form = block_admin_configure($form, $form_state, 'slideshow_block', NULL);
  return $form;
}

/**
 * Validate function for slideshow_block add form
 * @param $form
 * @param $form_state
 * @return unknown_type
*/
function slideshow_block_add_block_validate($form, &$form_state) {
  $slideshow_id = $form_state['values']['slideshow_name'];

  // Check if ID form is OK
  if (preg_match('/[^a-zA-Z0-9_]/', $slideshow_id)) {
   form_error($form['settings']['slideshow_name'], t('Slideshow ID must be alphanumeric or underscore only.'));
   return;  
  }

  $result = db_select('slideshow', 's')
              ->fields('s',array('cid'))
              ->where('name=LOWER(:name)', array(':name'=>$slideshow_id))
              ->execute()
              ->fetchField();

  // If result found, block name already found on DB
  if ($result) {
    form_error($form['settings']['slideshow_name'], t('This block name already exist.'));
    return;
  }
}


/**
 * Submit function for create slideshow_block
*/
function slideshow_block_add_block_submit($form, &$form_state) {
  // Creating slideshow block object
  $slideshow = new stdClass();
  $slideshow->name = $form_state['values']['slideshow_name'];
  $slideshow->width = $form_state['values']['slideshow_params']['width'];
  $slideshow->height = $form_state['values']['slideshow_params']['height'];
  $slideshow->speed = $form_state['values']['slideshow_params']['speed'];
  $slideshow->autoplay = $form_state['values']['slideshow_params']['autoplay'];
  $slideshow->exposed = $form_state['values']['slideshow_params']['exposed'];
  $slideshow->navigation = $form_state['values']['slideshow_params']['navigation'];

  // Saving information into DB
  drupal_write_record('slideshow', $slideshow);

  // Creating picture
  if ($file = file_save_upload('new_picture', array(), SLIDESHOW_BLOCK_PICTURE_FOLDER_PATH, FILE_EXISTS_REPLACE)) {

    // Set the file permanent
    $file->status   = FILE_STATUS_PERMANENT;
    $file = file_save($file);

    // Creating picture object
    $object = new stdClass();
    $object->cid = $slideshow->cid;
    $object->filepath = $file->destination;
    // Saving information into DB
    drupal_write_record('slideshow_pictures', $object);
  }

  drupal_set_message(t("Slideshow block successfully created"));

  $form_state['redirect'] = "admin/structure/block/manage/slideshow_block/{$slideshow->cid}/configure";
}

/**
 * @param $form
 * @param $form_state
 * @param $cpid
 * @return unknown_type
*/
function slideshow_block_delete_picture_confirm($form, &$form_state, $cpid) {
  $form = array();

  $element = db_select('slideshow_pictures', 'sp')
               ->fields('sp',array('cid','filepath'))
               ->condition('cpid', $cpid)
               ->execute()
               ->fetchObject();

  $form['items'] = array(
    '#markup' => '<div><img src="'.file_create_url($element->filepath).'"></div>', 
  );

  $form['cpid'] = array(
    '#type'  => 'hidden',
    '#value' => $cpid,
  );

  $form['cid'] = array(
    '#type'  => 'hidden',
    '#value' => $element->cid,
  );

  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
  $form['#submit'][] = 'slideshow_delete_picture_confirm_submit';

  return confirm_form(
             $form,
             t('Are you sure you want to delete this item?'),
             "admin/structure/block/manage/slideshow_block/{$element->cid}/configure",
             t('This action cannot be undone.'),
             t('Delete all'),
             t('Cancel'));
  
}

/**
 * @param $form
 * @param $form_state
 * @return unknown_type
*/
function slideshow_delete_picture_confirm_submit($form, &$form_state) {
  $cpid = $form_state['values']['cpid'];
  $cid  = $form_state['values']['cid'];

  db_delete('slideshow_pictures')
    ->condition('cpid', $cpid)
    ->execute();

  drupal_set_message(t('Picture deleted for this slideshow.'));

  $form_state['redirect'] = "admin/structure/block/manage/slideshow_block/{$cid}/configure";
}
