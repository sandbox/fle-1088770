<?php
// $Id: $
/**
 * @file slideshow-block-display.tpl.php
 * Display of slideshow block
 *
 * Variables available:
 * - $pictures: Array of slideshow picrures
 * - $dom_id: Id of slideshow html tag (used for JS)
 * - $width : Width of the slideshow
 * - $height: Height of the slideshow
 * - $classes: Classes to apply to the div wrapper
 * - $navigation: Where to display navigation buttons
 */
?>

<div class="<?php print $classes?>" <?php print $attributes; ?> style="width:<?php print $width; ?>px">

  <?php if($navigation==SLIDESHOW_BLOCK_NAVIGATION_ABOVE) { ?>
    <div class="navi"></div>
  <?php } ?>

  <div id="<?php print $dom_id; ?>" class="slideshow-block" style="height:<?php print $height; ?>px">

    <div class="items">
      <?php foreach ($pictures as $picture) { ?>
        <div>
          <img src="<?php print $picture['src']; ?>" alt="<?php print $picture['description']; ?>" />
          <?php if (!empty($picture['description'])) { ?>
            <div class="slideshow-block-picture-description">
              <div><?php print $picture['description']; ?></div>
            </div>
          <?php } ?>
        </div>
      <?php } ?>
    </div>

  </div>

  <?php if($navigation==SLIDESHOW_BLOCK_NAVIGATION_BELOW) { ?>
    <div class="navi"></div>
  <?php } ?>

</div>