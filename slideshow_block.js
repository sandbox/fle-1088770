(function ($) {
  Drupal.behaviors.slideshowBlockBehavior = {
    attach: function(context, settings) {
      $(".slideshow-block", context).each(function(i){
        slideshow_settings = Drupal.settings[$(this).attr('id')];
        $(this).scrollable({circular: true, speed:slideshow_settings['speed']}).navigator();
        if(slideshow_settings['autoplay']==1)
          $(this).autoscroll({interval:slideshow_settings['exposed']});
      });
    }
  };
})(jQuery);
